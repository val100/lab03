# Ansible Workshop
Lab 03: Starting with Playbooks

---

# Preparations

## Prepare hosts file - open host file

```
$ sudo vim /etc/ansible/hosts
```

## Paste the following snippet in the file - nodeip will be replaced by the managed node ip(or ip's) - <bold>if the hosts file is already updated, skip this step</bold>

```
[demoservers]
(nodeip(s))
```

# Instructions

- Browser to port 80 of managed host to verify its empty
- Create a new playbook
- Paste definition
- Run command to verify syntax
- Run playbook
- Open browser and navigate to port 80 of managed host

## Browse to port 80 in host ip (in browser) to see it is empty : 

```
http://(nodeip):80
```

## Create a new playbook

```
$ sudo vim playbook.yml
```

## Paste the following definition

```
- hosts: demoservers
  become_method: sudo
  become: yes
  remote_user: sela
  tasks:
  - name: ensure apache is at the latest version
    apt:
      name: apache2
      state: latest
    notify:
    - restart apache2
  - name: ensure apache is running
    service:
      name: apache2
      state: started
  handlers:
    - name: restart apache2
      service:
        name: apache2
        state: restarted
```

## Save and quit
```
press esc
press :wq
```

## Run command to verify syntax in the playbook file

```
ansible-playbook playbook.yml --syntax-check
```

## Run playbook

```
ansible-playbook playbook.yml
```

## Open port 80 on host ip and show the default apache page

```
http://(nodeip):80 
```
